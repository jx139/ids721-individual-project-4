use lambda_runtime::{service_fn, Error, LambdaEvent};
use log::LevelFilter;
use serde::Deserialize;
use serde_json::{json, Value};
use simple_logger::SimpleLogger;

#[derive(Deserialize)]
struct EventData {
    sales: Vec<f64>, // Assuming sales data is a list of floating point numbers
}

async fn function_handler(event: LambdaEvent<Value>) -> Result<Value, Error> {
    SimpleLogger::new().with_level(LevelFilter::Info).init()?;
    println!("Processing event: {:?}", event.payload);

    // Deserialize the event payload into EventData
    let data: EventData = serde_json::from_value(event.payload)?;

    // Calculate total sales
    let total_sales: f64 = data.sales.iter().sum();

    // Return the total sales as a JSON response
    Ok(json!({"total_sales": total_sales}))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(service_fn(function_handler)).await?;
    Ok(())
}
