# Rust AWS Lambda and Step Functions
![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-individual-project-4/badges/main/pipeline.svg)

This project demonstrates how to set up an AWS Lambda function written in Rust, coordinated by AWS Step Functions to orchestrate a data processing pipeline. The Lambda function processes sales data by calculating total sales and returning this data. You can see the demo [video](https://drive.google.com/file/d/1cL2lrY-VEZOOYI76W_HxdFFi9zkzdchg/view?usp=sharing) here.

## Requirements

1. **Rust AWS Lambda Function**: A serverless function to process sales data.
2. **Step Functions Workflow**: AWS Step Functions to coordinate multiple Lambda functions.
3. **Data Processing Pipeline**: Orchestrate the data processing flow using Step Functions.

## Setup

### Part 1: Setting up the Rust AWS Lambda Function

#### Tools and Prerequisites

- Rust programming language
- AWS CLI configured with appropriate AWS access credentials
- An IAM role for the Lambda function with permissions to log to Amazon CloudWatch

#### Steps

1. **Create Rust Project**:
    - Initialize a new Rust project by running:
      ```bash
      cargo new lambda_function
      cd lambda_function
      ```

2. **Implement the Lambda Function**:
    - Use the provided `main.rs` code to process sales data.

3. **Build and Deploy**:
    - Compile the Rust project:
      ```bash
      cargo build --release
      ```
    - Package the compiled binary for Lambda deployment:
      ```bash
      cp target/release/lambda_function bootstrap && zip lambda.zip bootstrap
      ```
    - Deploy the function to AWS Lambda:
      ```bash
      aws lambda create-function --function-name rustLambdaFunction \
        --handler unused --zip-file fileb://lambda.zip \
        --runtime provided.al2 --role arn:aws:iam::YOUR_ACCOUNT_ID:role/lambda-execution-role
      ```

    ![iam](./pic/iam.png)
    ![lambda1](./pic/lambda1.png)
    ![lambda2](./pic/lambda2.png)
    ![lambda3](./pic/lambda3.png)

### Part 2: Creating the AWS Step Functions Workflow

1. **Define the State Machine**:
    - Create a JSON file `state_machine.json` with the following content to orchestrate the Lambda function:
      ```json
      {
        "StartAt": "rustLambdaFunction",
        "States": {
          "rustLambdaFunction": {
            "Type": "Task",
            "Resource": "arn:aws:lambda:REGION:ACCOUNT_ID:function:rustLambdaFunction",
            "End": true
          }
        }
      }
      ```

    ![machine](./pic/machine.png)

2. **Deploy the State Machine**:
    - Ensure you have an IAM role (`StepFunctions-ExecutionRole`) with permission to execute the Lambda function.
    - Create the state machine using AWS CLI:
      ```bash
      aws stepfunctions create-state-machine --name "RustLambdaStateMachine" \
        --definition file://state_machine.json \
        --role-arn "arn:aws:iam::ACCOUNT_ID:role/StepFunctions-ExecutionRole"
      ```

    ![step_create](./pic/step_create.png)

### Part 3: Orchestrate the data processing flow using Step Functions

1. **Start an Execution of the State Machine**:
    - Use the AWS Management Console or AWS CLI to start an execution and monitor the results.
    ![step_exe](./pic/step_exe.png)

2. **Describe the output of an Execution of the State Machine**:
    - Use the AWS Management Console or AWS CLI to start an execution and monitor the results.
    ![step_output](./pic/step_output.png)

## Conclusion

This project illustrates a complete setup for processing data with a Rust-written AWS Lambda function and coordinating workflows using AWS Step Functions. This solution leverages AWS services to process and handle data efficiently in a serverless architecture.
